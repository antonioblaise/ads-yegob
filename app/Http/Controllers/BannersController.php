<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Activity;
use App\Stats;
use App\Location;

class BannersController extends Controller
{
    public function show(Request $request, $id){
        $banner = Banner::find($id); // find banner with id
        if($banner != null){
            $location = new Location(request()->ip());

            Stats::create([
                "ip" => $location->getIp(),
                "country" => $location->getCountry(),
                "country_code" => $location->getCountryCode(),
                "city" => $location->getCity(),
                "region" => $location->getRegion(),
                "isp" => $location->getISP(),
                "timezone" => $location->getTimezone(),
                "affiliator" => $request->input("affiliator", "0"),
                "ads_id" => $id
            ]);



            $table->ipAddress('ip');
            $table->string('country')->nullable();
            $table->string('country_code')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('isp')->nullable();
            $table->string('timezone')->nullable();
            $table->bigInteger('affiliator');
            $table->bigInteger('ads_id');

            return view('embed', ['banner' => $banner]);
        }else{
            return "Banner doesn't exists.";
        }
    }

    public function redirect(Request $request, $id){
        $banner = Banner::find($id); // find banner with id
        if($banner != null){
            $location = new Location(request()->ip());

            Stats::create([
                "ip" => $location->getIp(),
                "country" => $location->getCountry(),
                "country_code" => $location->getCountryCode(),
                "city" => $location->getCity(),
                "region" => $location->getRegion(),
                "isp" => $location->getISP(),
                "timezone" => $location->getTimezone(),
                "affiliator" => $request->input("affiliator", "0"),
                "ads_id" => $id
            ]);
            
            return redirect()->away($banner->link);
        }else{
            return "Banner doesn't exists.";
        }    
    }
}
