<?php

namespace App\Http\Controllers\Influencer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
//use

use App\User;

class AccountController extends Controller
{
    public function show(Request $request){
        // show user informations
        $account = Auth::user();
        return view('influencer.account.show', ['account' => $account]);
    }

    public function update(Request $request){
        // update user informations
        $account = Auth::user();

        $user = User::find($account->id);

        if(!empty($request->input('password', '')) && $request->password == $request->cpassword){
            $user->password = bcrypt($request->password);
        }   
        
        $user->name = $request->name;
        $user->save();

        return redirect('/influencer/account');
    }
}
