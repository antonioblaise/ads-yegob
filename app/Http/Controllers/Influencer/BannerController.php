<?php

namespace App\Http\Controllers\Influencer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Banner;
use App\User;

class BannerController extends Controller
{
    public function index(Request $request){
        $count = $request->input("count", 10);
        $banners = Banner::where()->paginate($count);

        return view('influencer.banners.index');
    }
}
