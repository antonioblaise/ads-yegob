<?php

namespace App\Http\Controllers\Influencer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Stats;
use App\User;

class StatsController extends Controller
{
    public function index(Request $request){
        $now = Carbon::now();
        $start = $now->startOfMonth()->toDateString(). " 00:00:00";        ;
        $end = $now->endOfMonth()->addDay()->toDateString(). " 00:00:00";

        $stats = Stats::where("affiliator","=", Auth::user()->id)
            ->where("created_at", ">=", $start)
            ->where("created_at", "<", $end)
            ->get()
            ->groupBy(function($item){
                    return $item->created_at->format('d-M-y');
            })->toArray();
            
        return view('influencer.stats.index', ['stats' => $stats]);
    }
}
