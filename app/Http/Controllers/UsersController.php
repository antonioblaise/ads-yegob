<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class UsersController extends Controller
{
    //
    public function __construct(){

    }

    public function login(Request $request){
        // if it is a get request
        if($request->isMethod('get')){
            $user = Auth::user();

            if(isset($user)){
                return redirect('/'.$user->role);
            }

            return view('login');
        }else{
            $remember = isset($request->remember) ? true: false;

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password ], $remember)){
                $user = Auth::user();
                if(isset($user)){
                    return redirect('/'.$user->role);
                }
            }else{
                return redirect('/login')->withErrors(['error' => 'your email or password is incorrect']);
            }
        }
    }

    public function register(Request $request){
        return "User Registration is closed for the moment.";
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
}
