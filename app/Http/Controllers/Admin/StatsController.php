<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Stats;
use App\Affiliator;
use Carbon\Carbon;

class StatsController extends Controller
{
    //
    public function __construct(){

    }

    public function index(Request $request){
        $search = $request->input('query', '');
        $affiliators = Affiliator::all();
        return view('admin.stats.index', ["affiliators" => $affiliators]);
    }

    public function show(Request $request, $id){
        $now = Carbon::now();
        $start = $now->startOfWeek()->toDateString(). " 00:00:00";        ;
        $end = $now->startOfWeek()->addWeek()->toDateString(). " 00:00:00";

        $stats = Stats::where("affiliator","=", $id)
            ->where("created_at", ">=", $start)
            ->where("created_at", "<", $end)
            ->get()
            ->groupBy(function($item){
                    return $item->created_at->format('d-M-y');
            })->toArray();

            return view('admin.stats.show', ['stats' => $stats ]);
    }

    public function showBanner(Request $request, $id, $banner){
        $stats = Stats::where("affiliator", $id);


    }
}