<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Client;


class ClientsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', 10);
        $clients = Client::paginate($count);
        return view('admin.clients.index', ['clients' => $clients ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return something
        $client = Client::create([
            'name' => $request->name,
            'website' => $request->website,
            'description' => !empty($request->description) ? $request->description : " ",
            'created_by' => Auth::user()->id,
	        'updated_by' => Auth::user()->id,
            'status' => $request->input('status', false) ? true : false
        ]);

        return redirect('/admin/clients/'.$client->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        return view('admin.clients.show', ['client' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('admin.clients.edit', ['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        
        $client->name = $request->name;
        $client->website = $request->website;
        $client->description = $request->description;
        $client->status = $request->input('status', false) ? true : false;
	$client->updated_by = Auth::user()->id;
        $client->save();

        return redirect('/admin/clients/'.$client->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $name = $client->name;
        $client->destroy();
        return redirect('/admin/clients')->with('message', 'client '.$name.' successfuly deleted.');
    }
}
