<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Banner;
use App\Client;
use App\Affiliator;

class BannersController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', 10);
        $banners = Banner::paginate($count);
        return view('admin.banners.index', ['banners' => $banners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $affiliators = User::where('role', 'influencer')->get();
        return view('admin.banners.create', ['clients' => $clients, 'affiliators' => $affiliators]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // process upload image
        $path = "";
        $image = $request->image;
        if($request->hasFile('image')){
            $path = $image->store('images', 'public');
        }

        $banner = Banner::create([
            'title' => $request->title,
            'link' => $request->link,
            'image' => $path,
            'publisher_id' => $request->publisher_id,
            'client_id' => $request->client_id,
            'width' => $request->input('width', '728'),
            'height' => $request->input('height', '90'),
            'affiliator' => $request->input('affiliator'),
            'price' => $request->input('price'),
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'status' => $request->input('status', false) ? true : false
        ]);

        return redirect('/admin/banners/'.$banner->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Banner::find($id);
        return view('admin.banners.show', ['banner' => $banner]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        $clients = Client::all(['id', 'name']);
        $affiliators = Affiliator::all();

        return view('admin.banners.edit', ['banner' => $banner, 'clients' => $clients, 'affiliators' => $affiliators]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // process image
        $image = $request->image;
        $path = "";
        if($request->hasFile('image')){
            $path = $image->store('images', 'public');
        }

        $banner = Banner::find($id);

        $banner->title = $request->title;
        $banner->link = $request->link;
        
        if(empty($path) != false){
            $banner->image = $path;
        }

        $banner->client_id = $request->client_id;
        $banner->publisher_id = $request->publisher_id;
        $banner->updated_by = Auth::user()->id;
        $banner->width = $request->input('width', '728');
        $banner->height = $request->input('height', '90');
        $banner->price = $request->input('price', 0);
        $banner->status = $request->input('status', false) ? true : false;
        $banner->affiliator = $request->input('affiliator');
        
        $banner->save();

        return redirect('/admin/banners/'.$banner->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();
        return redirect('/admin/banners')->with('message', 'Banner successfully deleted');
    }
}
