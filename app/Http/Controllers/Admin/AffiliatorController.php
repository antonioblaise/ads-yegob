<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class AffiliatorController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', 10);
        $affiliators = User::where("role", "influencer")->paginate($count);
        return view('admin.affiliators.index', ['affiliators' => $affiliators]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.affiliators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $affiliator = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role' => $request->input('role')
        ]);

        return redirect('/admin/affiliators/'.$affiliator->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $affiliator = User::find($id);

        return view('admin.affiliators.show', ['affiliator' => $affiliator]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $affiliator = User::find($id);
        return view('admin.affiliators.edit', ['affiliator' => $affiliator]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $affiliator = User::find($id);

        $affiliator->name = $request->input('name');
        $affiliator->email = $request->input('email');

        if(isset($request->password)){
            $affiliator->password = bcrypt($request->password);
        }
        
        $affiliator->role = $request->input('role');

        $affiliator->save();
        
        return redirect('/admin/affiliators/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affiliator = User::find($id);
        if($affiliator){
            $affiliator->delete();
        }
        
        return redirect('/admin/affiliators/');
    }
}


