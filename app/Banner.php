<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Client;

class Banner extends Model
{
    protected $guarded = ['id'];

    public function author(){
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function client(){
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function publisher(){
        return $this->hasOne('App\Client', 'id', 'publisher_id');
    }
}
