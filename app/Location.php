<?php
    namespace App;

    class Location{

        private $response;
        private $status;
        private $coordinates;
        private $ip;
        private $countryCode;
        private $country;
        private $city;
        private $region;
        private $isp;
        private $timezone;

        public function __construct($ip = ""){
            $api = "http://ip-api.com/json/".$ip;
            $response = file_get_contents($api, true);

            if($response){
                $response = json_decode($response, true);
                $this->response = $response;
                $this->status = $response["status"];
                
                if($response["status"] == "success"){
                    $this->coordinates = array(
                        "longitude" => $response["lon"],
                        "latitude" => $response["lat"],
                    );
    
                    $this->ip = $response["query"];
                    $this->countryCode = $response["countryCode"];
                    $this->country = $response["country"];
                    $this->city = $response["city"];
                    $this->region = $response["regionName"];
                    $this->isp = $response["isp"];
                    $this->timezone = $response["timezone"];
                }
            }


        }

        public function getResponse(){
            return $this->response;
        }

        public function isValid(){
            return !is_null($this->status) && $this->status == "success";
        }

        public function getCoordinates(){
            return $this->coordinates;
        }

        public function getIp(){
            return $this->ip;
        }

        public function getCountryCode(){
            return $this->countryCode;
        }

        public function getCountry(){
            return $this->country;
        }

        public function getCity(){
            return $this->city;
        }

        public function getRegion(){
            return $this->region;
        }

        public function getISP(){
            return $this->isp;
        }

        public function getTimezone(){
            return $this->timezone;
        }

    }
?>