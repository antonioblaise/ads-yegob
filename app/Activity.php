<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = "activits";
    protected $fillable = ['banner_id', 'ip_address', 'activity'];
}
