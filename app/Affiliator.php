<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliator extends Model
{
    //
    protected $table = 'affiliator';
    
    protected $guarded = ['id'];
}
