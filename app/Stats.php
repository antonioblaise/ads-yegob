<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    //
    protected $table = "stats";
    protected $guarded = ["id"];

    protected $hidden = ['password'];
}
