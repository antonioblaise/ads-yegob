<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Client extends Model
{
    //
    protected $guarded = ['id'];


    public function author(){
        return $this->hasOne('App\User', 'id', 'created_by');
    }

}
