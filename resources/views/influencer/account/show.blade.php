@extends('partials.influencer')

@section("title", "My Account")

@section("content")

<div class="row">
    <div class="col-md-8">
        <h4>My Account Information</h4>
        <hr>
        {{ Form::open() }}
            <div class="form-group">
                <label for="name">Full Name:</label>
                {{ Form::text('name', $account->name, ['class' => 'form-control', 'id' => 'name']) }}
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                {{ Form::text('email', $account->email, ['class' => 'form-control', 'id' => 'email', 'readonly' => '']) }}
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                {{ Form::password('password',['class' => 'form-control', 'id' => 'password']) }}
            </div>

            <div class="form-group">
                <label for="cpassword">Confirm Password:</label>
                {{ Form::password('cpassword',['class' => 'form-control', 'id' => 'cpassword']) }}
            </div>
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection