@extends('partials.authenticate')

@section('title', 'Login Page')


@section('content')
    <div class="col-md-6 col-md-offset-3">
        <br>
        <br>
        <br>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h5>Login</h5>
            </div>
            <div class="panel-body">
            {!! Form::open(['url' => '/login', 'method' => 'POST']) !!}
                <div class="form-group">
                    <label for="email">Email:</label>
                    {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) }}
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
                </div>
                
                <div class="checkbox">
                    <label>
                        {{ Form::checkbox('remember', '1') }} 
                        Remember me ?
                    </label>
                </div>
                <div class="form-group">
                    {{ Form::submit('Login', ['class' => 'btn btn-info']) }}
                    <a href="/register" class="btn btn-success">Register</a>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection