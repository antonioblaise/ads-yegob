@extends('partials.admin')

@section('title', 'List all banners')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>title</th>
                    <th>Client</th>
                    <th>Publisher</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Status</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($banners as $banner)
                    <tr>
                        <th>
                            <a href="{{ route('banners.edit', ['id' => $banner->id ]) }}">{{ $banner->title }}</a>
                        </th>
                        <th>
                            <a href="{{ route('clients.edit', ['id' => $banner->client->id ] ) }}">
                                {{ $banner->client->name }}
                            </a>
                        </th>
                        <th>
                            <a href="{{ route('clients.edit', ['id' => $banner->client->id ] ) }}">
                                {{ $banner->publisher->name }}
                            </a>
                        </th>
                        <th>{{ $banner->author->name }}</th>
                        <th>{{ $banner->created_at }}</th>
                        <th>{{ $banner->status }}</th>
                        <th>
                            <a href="{{ route('banners.edit', ['id' => $banner->id ]) }}" class="btn btn-info">edit</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection