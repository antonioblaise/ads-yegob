@extends('partials.admin')

@section('title', 'Edit a Banner')

@section('content')

<div class="row">
    <div class="col-md-8">
        <h3>Edit Banner</h3>
        <hr>
        {{ Form::model($banner, ['route' => ['banners.update', $banner->id], 'files' => true]) }}
            <input type="hidden" name="_method" value="PUT" />
            <div class="form-group">
                <label for="title">Title:</label>
                {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
            </div>
            <div class="form-group">
                <label for="link">Link:</label>
                {{ Form::text('link', null, ['class' => 'form-control', 'id' => 'link']) }}
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                {{ Form::file('image') }}
            </div>

            <div class="form-group">
                <label for="width">Width:</label>
                {{ Form::text('width', '728', ['class' => 'form-control', 'id' => 'width']) }}
                <br>
                <label for="height">Height:</label>
                {{ Form::text('height', '90', ['class' => 'form-control', 'id' => 'height']) }}
            </div>
            <div class="form-group">
                <label for="client">client:</label>
                <select name="client_id" id="client" class="form-control">
                    @foreach($clients as $client)
                        @if($client->id == $banner->client_id)
                            <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                        @else
                            <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="publisher">Publisher:</label>
                <select name="publisher_id" id="publisher" class="form-control">
                    @foreach($clients as $client)
                        @if($client->id == $banner->client_id)
                            <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                        @else
                            <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="affiliator">Affiliator:</label>
                <select name="affiliator" id="affiliator" class="form-control">
                    @foreach($affiliators as $affiliator)
                        @if($affiliator->id == $banner->affilitor)
                            <option value="{{ $$affiliator->id }}" selected>{{ $$affiliator->last_name }} {{ $$affiliator->first_name }}</option>
                        @else
                            <option value="{{ $affiliator->id }}">{{ $affiliator->last_name }} {{ $affiliator->first_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                {{ Form::text("price", null, ['class' => 'form-control', 'id' => 'price']) }}
            </div>

            <div class="form-group">
                <label for="status">Status:</label>
                {{ Form::select('status', ['0' => 'unpublished', '1' => 'published'], '1', ['class' => 'form-control','id' => 'status']) }}
            </div>
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
    <div class="col-md-4">
        <h3>Banner</h3>
        <div class="banner form-group">
            <img src="{{ asset('storage/'.$banner->image) }}" alt="" class="img-responsive">
        </div>
        <hr>
            <button class="btn btn-primary" data-toggle="modal" data-target="#htmlembed">Embed HTML Code</button>
        <hr>
        {{ Form::open(['route' => ['banners.destroy', $banner->id]]) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
        {{ Form::close() }}
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="htmlembed">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Banner HTML Code</h4>
                </div>
                <div class="modal-body">
                    <span>Code iframe HTML</span>
                    <code>
                            &lt;iframe src=&quot;http://ads.yegob.com/banner/embed/1&quot; frameborder=&quot;0&quot; width=&quot;{{ $banner->width }}&quot; height=&quot;{{ $banner->height}}&quot; scrolling=&quot;no&quot;&gt;&lt;/iframe&gt;
                    </code>
                    <hr>
                    <span>Lien Affiliator:</span>
                    <code>
                        http://ads.yegob.com/advert/{{ $banner->id }}?affiliator={{ $banner->affiliator }}
                    </code>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
