@extends('partials.admin')

@section('title', 'Create a new Banner')

@section('content')

<div class="row">
    <div class="col-md-8">
        <h3>Create a Banner</h3>
        <hr>
        {{ Form::open(['route' => 'banners.store', 'files' => true]) }}
            <div class="form-group">
                <label for="title">Title:</label>
                {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
            </div>
            <div class="form-group">
                <label for="link">Link:</label>
                {{ Form::text('link', null, ['class' => 'form-control', 'id' => 'link']) }}
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                {{ Form::file('image') }}
            </div>
            <div class="form-group">
                <label for="width">Width:</label>
                {{ Form::text('width', '728', ['class' => 'form-control', 'id' => 'width']) }}
                <br>
                <label for="height">Height:</label>
                {{ Form::text('height', '90', ['class' => 'form-control', 'id' => 'height']) }}
            </div>
            
            <div class="form-group">
                <label for="client">client:</label>
                <select name="client_id" id="client" class="form-control">
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="publisher">Publisher:</label>
                <select name="publisher_id" id="publisher" class="form-control">
                    @foreach($clients as $client)
                        <option value="{{ $client->id }}">{{ $client->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="affiliator">Affiliator:</label>
                <select name="affiliator" id="affiliator" class="form-control">
                    @foreach($affiliators as $affiliator)
                        <option value="{{ $affiliator->id }}">{{ $affiliator->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                {{ Form::text("price", null, ['class' => 'form-control', 'id' => 'price']) }}
            </div>

            <div class="form-group">
                <label for="status">Status:</label>
                {{ Form::select('status', ['0' => 'unpublished', '1' => 'published'], '1', ['class' => 'form-control','id' => 'status']) }}
            </div>
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection