@extends('partials.admin')

@section('title', 'Create a new client')

@section('content')
    <div class="row">
        <div class="col-md-8">
            {{ Form::open(['route' => 'clients.store']) }}
                <div class="form-group">
                    <label for="name">Name:</label>
                    {{ Form::text('name', null, ['class' => 'form-control', 'id' =>'name']) }}
                </div>

                <div class="form-group">
                    <label for="website">Website:</label>
                    {{ Form::text('website', null, ['class' => 'form-control', 'id' => 'website']) }}
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="status">Status:</label>
                    {{ Form::select('status', ['0' => 'unpublished', '1' => 'published'], '1', ['id' => 'status', 'class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection