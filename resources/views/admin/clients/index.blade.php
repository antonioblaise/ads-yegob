@extends('partials.admin')

@section('title', 'All Clients')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Website</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Status</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                    <tr>
                        <th><a href="{{ route('clients.edit', $client->id)}}">{{ $client->name }}</a></th>
                        <th>{{ $client->website }}</th>
                        <th>{{ $client->author->name }}</th>
                        <th>{{ $client->created_at }}</th>
                        <th>{{ $client->status }}</th>
                        <th><a href="{{ route('clients.edit', $client->id)}}" class="btn btn-info">Edit</a></th>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Website</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Status</th>
                    <th>Edit</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@endsection