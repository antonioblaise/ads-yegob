@extends('partials.admin')

@section('title', 'List all banners')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($affiliators as $affiliator)
                    <tr>
                        <th>
                            <a href="{{ $affiliator->id }}">{{ $affiliator->first_name }}</a>
                        </th>
                        <th>
                            <a href="{{ route('affiliators.edit', ['id' => $affiliator->id ] ) }}">
                                {{ $affiliator->last_name }}
                            </a>
                        </th>
                        <th>
                            {{ $affiliator->email }}
                        </th>
                        <th>
                            <a href="{{ route('affiliators.edit', ['id' => $affiliator->id ]) }}" class="btn btn-info">edit</a>
                            <a href="{{ url('/admin/stats/'.$affiliator->id) }}" class="btn btn-info">Show Statistics</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection