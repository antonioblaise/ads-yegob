@extends('partials.admin')

@section('title', 'Statistics')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Date: </th>
                    <th>Total Views</th>
                    <th>Revenues: </th>
                </tr>
            </thead>
            <tbody>
                @foreach(array_reverse($stats) as $date => $stat)
                <tr>
                    <td>{{ $date }}</td>
                    <td>{{ count($stat) }} views</td>
                    <td>0.0 RWF</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection