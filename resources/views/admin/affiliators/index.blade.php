@extends('partials.admin')

@section('title', 'List all banners')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Role</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($affiliators as $affiliator)
                    <tr>
                        <th>
                            <a href="{{ route('affiliators.edit', ['id' => $affiliator->id ] ) }}">
                                {{ $affiliator->name }}
                            </a>
                        </th>
                        <th>
                            {{ $affiliator->email }}
                        </th>
                        <th>{{ $affiliator->created_at }}</th>
                        <th>{{ $affiliator->role }}</th>
                        <th>
                            <a href="{{ route('affiliators.edit', ['id' => $affiliator->id ]) }}" class="btn btn-info">edit</a>
                        </th>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection