@extends('partials.admin')

@section('title', 'Create a new Influencer')

@section('content')

<div class="row">
    <div class="col-md-8">
        <h3>Create an Influencer</h3>
        <hr>
        {{ Form::open(['route' => 'affiliators.store', 'files' => true]) }}
            <div class="form-group">
                <label for="name">Full Name (*) :</label>
                {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
            </div>
            <div class="form-group">
                <label for="email">Email (*) :</label>
                {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) }}
            </div>
            <div class="form-group">
                <label for="password">Password (*) :</label>
                {{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                {{ Form::select('role', ['admin' => 'Administrator', 'influencer' => 'Influencer', 'client' => 'Client'], 'influencer', ['class' => 'form-control', 'id' => 'role'])}}
            </div>
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection