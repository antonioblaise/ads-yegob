@extends('partials.admin')

@section('title', 'Edit an affiliator')

@section('content')

<div class="row">
    <div class="col-md-8">
        <h3>Edit Influencer</h3>
        <hr>
        {{ Form::model($affiliator, ['route' => ['affiliators.update', $affiliator->id]]) }} 
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="name">Full Name (*) :</label>
                {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
            </div>
            <div class="form-group">
                <label for="email">Email (*) :</label>
                {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) }}
            </div>
            <div class="form-group">
                <label for="password">Password (*) :</label>
                {{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                {{ Form::select('role', ['admin' => 'Administrator', 'influencer' => 'Influencer', 'client' => 'Client'], null, ['class' => 'form-control', 'id' => 'role'])}}
            </div>
            <div class="form-group">
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
    <div class="col-md-4">
        <h3>Delete</h3>
        
        {{ Form::open(['route' => ['affiliators.destroy', $affiliator->id]]) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
        {{ Form::close() }}
    </div>
   
</div>
@endsection
