<html>
    <body>
        <style>
            html,body, *{
                margin: 0;
                padding: 0;
                border: 0px;
            }
        </style>
        <a href="/redirect/{{ $banner->id }}" target="_blank">
            <img src="{{ asset('storage/'.$banner->image) }}" alt="{{ $banner->title }}">
        </a>
    </body>
</html>