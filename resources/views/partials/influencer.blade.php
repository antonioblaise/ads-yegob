<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('css/app.css') }} ">
    <title>Admin - @yield('title') </title>
</head>
<body>
    <header id="header">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand">YegoB Marketing</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="navbar-nav nav">
                        <li class="nav-item">
                            <a href="/influencer/">Statistics</a>
                        </li>
                        <li class="nav-item">
                            <a href="/influencer/banners">Links</a>
                        </li>
                        <li class="nav-item">
                            <a href="/influencer/account">My Account</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="container">
        @yield('content')
    </div>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <p class="copyright">&copy; {{ date('Y') }} YegoB Ads Marketing</p>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- javascripts files -->
    @yield('jsfiles')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ url('js/app.js') }}"></script>
</body>
</html>