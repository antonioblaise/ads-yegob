<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

/* display and redirect routes */

Route::get('/banner/embed/{id}', 'BannersController@show');
Route::get('/advert/{id}', 'BannersController@redirect');

Route::any('/login', 'UsersController@login')->name('login');
Route::any('/register', 'UsersController@register')->name('register');

Route::get('/logout', 'UsersController@logout');

Route::group(['middleware' => ['auth', 'admin']], function(){
    Route::prefix('admin')->group(function () {
        Route::get('/', function(){
            return view('admin.dashboard');
        });

        Route::get('/stats', 'Admin\StatsController@index');
        Route::get('/stats/{id}', 'Admin\StatsController@show');
        Route::get('/stats/{id}/{banner}', 'Admin\StatsController@showBanner');

        // clients resource
        Route::resource('clients', 'Admin\ClientsController');
        // banners resource
        Route::resource('banners', 'Admin\BannersController');
        // affiliators resource
        Route::resource('affiliators','Admin\AffiliatorController');
    });
});

Route::group(['middleware' => ['auth', 'influencer']], function(){
    Route::prefix('influencer')->group(function(){
        Route::get('/', 'Influencer\StatsController@index');
        Route::get('/banners', 'Influencer\BannerController@index');
        Route::get('/account', 'Influencer\AccountController@show');
        Route::post('/account', 'Influencer\AccountController@update');
    });
});